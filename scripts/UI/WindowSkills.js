/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CWindowSkills ( x , y , w , h , game , container){
    this.container = container;
    CWindow.call(this, x , y , w , h , game );
    this.ctx.font = "12px monospace";
};

CWindowSkills.prototype = Object.create(CWindow.prototype);
CWindowSkills.prototype.constructor = CWindowSkills;


CWindowSkills.prototype.draw = function ( ){
    CWindow.prototype.draw.apply ( this );
    
    var face = this.game.faces.find ( this.game.faceIDSkillNames );    
    var faceExp = this.game.faces.find ( this.game.faceIDExpTable );    
    
    
    
    if ( ! ( face && faceExp) ){
        return;
    }
    
    
    
    if ( !face.loaded ){
        return;
    }

    
    var stats = this.game.myStats;
    var skillNames = face.data;
    var output = [];
    var bars = [];
    for (var i = CS_STAT_SKILLINFO ; i < CS_STAT_SKILLINFO + CS_NUM_SKILLS ; ++i) {

        if (!stats [ i ]){
            continue;
        }
        var name = skillNames[i-CS_STAT_SKILLINFO][0];
        var s = String.rpad(name, 30, " ");
        for ( var k =0 ; k < stats [ i ].length - 1 ; ++k ){
            s += ( String.rpad(stats [ i ][ k ] + "", 12, " ")) + " ";
        }
        output.push(s);
        var bar = new CProgressBar(this,'blue','lightblue', 200);
        
        var prevExp = faceExp.data [ parseInt ( stats [ i ] [ 0 ] )-1];
        var exp = parseInt ( stats [ i ] [ 1 ] );
        var nextExp = faceExp.data [ parseInt ( stats [ i ] [ 0 ] )];
        
        bar.currentVal = exp;
        bar.percent = ( exp / nextExp );
        
        
        
        bars.push ( bar );
        
    };
    
    this.resize( 240 , 45 * output.length+5 );
    this.ctx.fillStyle = "blue";
    this.ctx.fillRect ( 0 , 0 , this.w , this.h );
    this.ctx.fillStyle = "white";
    for (var i = 0; i< output.length; ++i) {
        
        this.ctx.fillText(output[i], 25, i * 45 + 10 );
        bars[i].x = 10;
        bars[i].y = i * 45 + 20;
        bars[i].draw(this.ctx);
    }
};
