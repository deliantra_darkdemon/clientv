/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CButton ( x , y , text , game , isActive){
    
    this.x = x;
    this.y = y;
    
    this.game = game;              
    this.paddingLeft = this.paddingRight = 5;
    this.setText(text);
    this.isActive = isActive;
};

CButton.prototype.setText = function ( txt ) {
    this.text = txt;    
    var w = this.paddingLeft + this.paddingRight + this.game.ctx.measureText(this.text).width;
    this.width = w;
    return this;
};

CButton.prototype.draw = function ( ctx ) {
    ctx.save ( );
    ctx.translate ( this.x, this.y );
    var w = this.paddingLeft + this.paddingRight + ctx.measureText(this.text).width;
    ctx.fillStyle = "brown";
    ctx.fillRect (0 , 0 , w , 30);
    ctx.strokeStyle = this.isActive ? "gold" : "blue";
    ctx.lineWidth = 4;
    ctx.strokeRect(0 , 0 , w , 30);
    ctx.fillStyle = "white";
    ctx.fillText(this.text, w*0.5 - ctx.measureText(this.text).width*0.5, 18);
    ctx.restore();
};