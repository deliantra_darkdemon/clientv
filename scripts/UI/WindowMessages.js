/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CWindowMessages ( x , y , w , h , game ){
    CWindow.call(this, x , y , w , h , game );
    var self = this;
    this.activeChannel = 0;
    this.canvas.height = 60;
    this.div.id = 'channels';
    
    this.input = this.appendTo.appendChild ( $_('input') );
    this.table2 = this.appendTo.appendChild ( $_('table') );
    this.table2.className = 'chat';
    this.input.className = 'inputbox';
    
    this.input.addEventListener('focus', function (){
        self.game.inputText = true;
    });
    
    this.input.addEventListener('blur', function (){
        self.game.inputText = false;
    });
    
    this.input.addEventListener('keypress', function ( e ){
        if (e.keyCode == CKeyboard.KEYS.ENTER){
            self.onMessage ( this.value );
            this.value = '';
        }
    });
    
    this.canvas.addEventListener ( 'click' , function ( e ) {
        self.onClick ( e );
    });
    
};

CWindowMessages.prototype = Object.create(CWindow.prototype);
CWindowMessages.prototype.constructor = CWindowMessages;

CWindowMessages.prototype.onMessage = function ( m ) {
    this.game.networking.send('command ' + this.game.channels[this.activeChannel].reply + " " + m);
};

CWindowMessages.prototype.onClick = function ( e ) {
    
    var w = 0;
    var self = this;
    this.activeChannel = 0;
    for (var i = 0; i < this.game.channels.length; ++i) {
        var s = "";
        if (typeof (this.game.channels[i]) == 'object') {
            s = this.game.channels[i].title;
            if (this.game.channels[i].active) {
                s += " *";
            }
        }

        var b = new CButton(w, 20, s, self.game);
        if (e.offsetX >= w && e.offsetX <= w + b.width + 2){        
            this.activeChannel = i;
            this.game.channels[this.activeChannel].active = false;
            this.settext();
            break;
        }
        w += b.width + 2;
        
    }
    
}

CWindowMessages.prototype.settext = function ( ) {
    this.table2.innerHTML = "";   
    var channel = this.game.channels [ this.activeChannel ];    
    for (var i = 0; i < channel.messages.length; ++i) {
        var msg = channel.messages [ i ];        
        var tr = this.table2.appendChild ( $_ ( 'tr' ) );
        this.table2.appendChild ( tr );
        var tdTime = tr.appendChild( $_( 'td' ) );
        tdTime.innerHTML = msg [ 0 ] + ":";
        var tdMsg = tr.appendChild( $_( 'td' ) );
        tdMsg.innerHTML = msg.slice(1) + "";        
    }
    
    if (channel.reply){
        this.input.style.display = 'block';
    }else{
        this.input.style.display = 'none';
    }
}

CWindowMessages.prototype.draw = function ( ){
    CWindow.prototype.draw.apply(this);

    var self = this;
    var w = 0;

    for (var i = 0; i < this.game.channels.length; ++i) {
        if ( ! typeof (this.game.channels[i]) == 'object') continue;
        
        var s = this.game.channels[i].title + (this.game.channels[i].active ? "*" : "");
        var b = new CButton(w, 5, s, self.game, i == this.activeChannel);
        b.draw(this.ctx);
        w += b.width + 2;
    }
    
    
    
};
