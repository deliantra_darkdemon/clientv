/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CWindowBars ( x , y , w , h , game ){
    CWindow.call(this, x , y , w , h , game );
    this.hpBar = new CProgressBar(this,'red','orange');
    this.hpBar.rotation = 0;//Math.PI + Math.PI*0.5;
    this.hpBar.x = 35;
    this.hpBar.y = 40;
    
    this.mpBar = new CProgressBar(this,'blue','lightblue');
    this.mpBar.rotation = this.hpBar.rotation;
    this.mpBar.x = this.hpBar.x;
    this.mpBar.y = this.hpBar.y + 30;
    
    this.foodBar = new CProgressBar(this,'green','lightgreen');
    this.foodBar.rotation = this.mpBar.rotation;
    this.foodBar.x = this.mpBar.x ;
    this.foodBar.y = this.mpBar.y + 30;
};

CWindowBars.prototype = Object.create(CWindow.prototype);
CWindowBars.prototype.constructor = CWindowBars;


CWindowBars.prototype.draw = function ( ){
    CWindow.prototype.draw.apply ( this );
    
    this.hpBar.currentVal = this.game.myStats [ CS_STAT_HP ];
    this.hpBar.maxVal = this.game.myStats [ CS_STAT_MAXHP ];
    
    this.hpBar.draw ( this.ctx );
    
    this.mpBar.currentVal = this.game.myStats [ CS_STAT_SP ];
    this.mpBar.maxVal = this.game.myStats [ CS_STAT_MAXSP ];
    
    this.mpBar.draw ( this.ctx );
    
    this.foodBar.currentVal = this.game.myStats [ CS_STAT_FOOD ];
    this.foodBar.maxVal = 999;    
    this.foodBar.draw ( this.ctx );
    
};
