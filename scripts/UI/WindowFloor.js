/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CWindowFloor ( x , y , w , h , game ){
    CWindow.call(this, x , y , w , h , game );
};

CWindowFloor.prototype = Object.create(CWindow.prototype);
CWindowFloor.prototype.constructor = CWindowFloor;


CWindowFloor.prototype.draw = function ( ){
    CWindow.prototype.draw.apply ( this );
    
    var floorContainer = this.game.containers.find(0);
    if (floorContainer){        
        this.resize( 220 , 25*floorContainer.items.length+5 );
        this.ctx.fillStyle = "blue";
        this.ctx.fillRect(0,0,220,25*floorContainer.items.length+5);
        
        for (var i = 0; i< floorContainer.items.length; ++i){
            /**             
             * @type CItem
             */
            var item = floorContainer.items[i];
            this.ctx.fillStyle="white";
            this.ctx.save( );
            this.ctx.translate(0 ,10);
            if (item.nr_of > 1){
                this.ctx.fillText(item.nr_of + " x " + item.namepl, 25,i*25+10);
            }else 
                this.ctx.fillText(item.name, 25,i*25+10);
            var img = this.game.findFace(item.face);
            if (img){
                this.ctx.drawImage(img, 0, i*25,20,20);
            }
            this.ctx.restore( );
        }
    }
};
