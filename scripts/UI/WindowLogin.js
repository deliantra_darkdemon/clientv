/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CWindowLogin ( x , y , w , h , game ){
    CWindow.call(this, x , y , w , h , game );
    var self = this;
    this.activeChannel = 0;
    this.canvas.height = 0;
    this.div.id = 'loginContainer';
    
    var header = this.appendTo.appendChild ( $_('h1') );
    header.appendChild ( document.createTextNode('D E L I A N T R A'));
    var table = this.table2 = this.appendTo.appendChild ( $_('table') );
    table.className = 'login';
    console.log ( table.className );
    
    var tr = this.table2.appendChild ( $_ ( 'tr' ) );
    var td = tr.appendChild ( $_ ( 'td' ) );
    var labelLogin  =  td.appendChild ( $_ ('h3') );
    labelLogin.innerHTML = 'LOGIN';
    td = tr.appendChild ( $_ ( 'td' ) );
    this.inputLogin = td.appendChild ( $_('input') );
    this.inputLogin.focus ( );
    tr = this.table2.appendChild ( $_ ( 'tr' ) );
    td = tr.appendChild ( $_ ( 'td' ) );
    var labelPassword  =  td.appendChild ( $_ ('h3') );
    labelPassword.innerHTML = 'PASSWORD';
    td = tr.appendChild ( $_ ( 'td' ) );
    this.inputPassword = td.appendChild ( $_('input') );
    this.inputPassword.type = 'password';
    
    tr = this.table2.appendChild ( $_ ( 'tr' ) );
    td = tr.appendChild ( $_ ( 'td' ) );
    td.colspan = 2;
    var inputGO = this.appendTo.appendChild ( $_('input') );
    inputGO.type = "button"; 
    inputGO.value = "GO";
    this.game.inputText = true;
    
    inputGO.addEventListener ( 'click', function ( ) {
        self.onGOClick();
    });
    
    this.inputPassword.addEventListener ( 'keypress' , function ( e ) {
        if ( e.keyCode == CKeyboard.KEYS.ENTER )
            self.onGOClick();
    } );
    
    this.inputLogin.addEventListener ( 'keypress' , function ( e ) {
        if ( e.keyCode == CKeyboard.KEYS.ENTER )
            self.onGOClick();
    } );
};

CWindowLogin.prototype = Object.create(CWindow.prototype);
CWindowLogin.prototype.constructor = CWindowLogin;



CWindowLogin.prototype.onGOClick = function ( ){
    if (this.inputLogin.length * this.inputPassword.length  == 0) {
        return;
    }
    this.game.start();
};
