/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CWindow ( x , y , w , h , game ){
    this.game = game;
    
    var storedSettings = localStorage.getItem ( "window_" + this.constructor.name );
    if (storedSettings) {
        storedSettings = JSON.parse ( storedSettings );
        x = parseInt ( storedSettings.x );
        y = parseInt ( storedSettings.y );
    }
    
    this.div = $_ ( 'div' );
    this.table = $_ ( 'table' );
    var tr = $_ ( 'tr' );
    this.table.appendChild(tr);
    td = $_ ( 'td' );
    td.className = 'tl';
    td.innerHTML = "&nbsp;";
    tr.appendChild(td);
    td = $_ ( 'td' );
    td.className = 'tm';
    tr.appendChild(td);
    td = $_ ( 'td' );
    td.className = 'tr';
    td.innerHTML = "&nbsp;";
    tr.appendChild(td);
    
    
    
    this.canvas = $_ ( 'canvas' );
    this.ctx = this.canvas.getContext( "2d" );
    
    tr = $_ ( 'tr' );
    this.table.appendChild(tr);
    td = $_ ( 'td' );
    td.className = 'ml';
    tr.appendChild(td);
    td = $_ ( 'td' );
    td.className = 'm';
    this.appendTo = td;
    this.appendTo.appendChild(this.canvas);
    tr.appendChild(td);
    td = $_ ( 'td' );
    td.className = 'mr';
    tr.appendChild(td);
    
    
    tr = $_ ( 'tr' );
    this.table.appendChild(tr);
    td = $_ ( 'td' );
    td.className = 'bl';
    tr.appendChild(td);
    td = $_ ( 'td' );
    td.className = 'bm';    
    tr.appendChild(td);
    td = $_ ( 'td' );
    td.className = 'br';
    tr.appendChild(td);
    
    
    this.div.appendChild(this.table);
    
    this.canvas.width = this.width = w;
    this.canvas.height = this.height = h;
    this.div.style.left = ( this.x = x ) + "px";
    this.div.style.top = ( this.y = y ) + "px";
    this.div.style.width = (this.width + 35 * 2) + "px";
    this.div.style.height = (this.height + 33 * 2) + "px";
    this.div.className = 'window';
    
    var self = this;
    
    this.canvas.addEventListener ( 'mousedown' , function ( e ){
        self.startDragging(e);        
    });
    
    if ( ! CWindow.globalHandler ){
        CWindow.globalHandler = true;
        window.addEventListener ( 'mousemove' , function ( e ){
            if ( ! CWindow.globalCurrentWindow || ! CWindow.globalCurrentWindow.startDrag ){
                return;
            }
            CWindow.globalCurrentWindow.drag ( e );
        });
        
        window.addEventListener ( 'mouseup' , function ( e ){
            if ( ! CWindow.globalCurrentWindow ) {
                return;
            }
            CWindow.globalCurrentWindow.endDrag(e);
        });
    }
    document.body.appendChild ( this.div );
};

CWindow.globalHandler = false;
CWindow.globalCurrentWindow = null;

CWindow.prototype.resize = function ( w , h ) {
    w = Math.min (1999, Math.max ( 10, w ) );
    h = Math.min (1999, Math.max ( 10, h ) );    
    this.canvas.width = this.width = w;
    this.canvas.height = this.height = h;
    this.div.style.width = (this.width + 35 * 2) + "px";
    this.div.style.height = (this.height + 33 * 2) + "px";            
};

CWindow.prototype.draw = function ( ) {
    this.ctx.clearRect ( 0 , 0 , this.canvas.width, this.canvas.height );
};

CWindow.prototype.show = function ( b ) {
    this.div.style.display = b ? "block" : "none";
};

CWindow.prototype.isShown = function ( ) {
    return this.div.style.display == "block";
};

CWindow.prototype.toggleShow = function ( ) {
    this.show(!this.isShown());
};

function dist (p,e) {
    return Math.sqrt( (p.x-e.y)*(p.x-e.y) + (p.y-e.y)*(p.y-e.y));
}

CWindow.prototype.startDragging = function ( e ) {
    this.isResize = dist ({x:e.offsetX,y:e.offsetY},{x:this.width,y:this.height}) < 20;
    this.startDrag = e;
    CWindow.globalCurrentWindow = this;  
};

CWindow.prototype.endDrag = function ( e ) {
    var w = this;    
    if (w.isResize) {
        this.resize( this.width + ( e.clientX - this.startDrag.clientX ) , this.height + ( e.clientY - this.startDrag.clientY ) );
    } else {
        w.x = parseInt( w.div.style.left );
        w.y = parseInt( w.div.style.top );
    }
    var settingName = "window_" + w.constructor.name;
    var settingValue = JSON.stringify ( { x : w.x , y : w.y , w : w.width , h : w.height });
    localStorage.setItem ( settingName , settingValue );
    w.startDrag = null;
    CWindow.globalCurrentWindow = null;
};

CWindow.prototype.drag = function ( e ) {
    if ( this.isResize == false ) {
        this.div.style.left = ( this.x + ( e.clientX - this.startDrag.clientX ) ) + 'px';
        this.div.style.top = ( this.y + ( e.clientY - this.startDrag.clientY ) ) + 'px';
    } else {
        this.div.style.width = ( this.width + ( e.clientX - this.startDrag.clientX ) ) + 'px';
        this.div.style.height = ( this.height + ( e.clientY - this.startDrag.clientY ) ) + 'px';
        
        //this.draw();
    }
}

CWindow.prototype.center = function ( e ) {
    this.move ( window.innerWidth*0.5 - this.width * 0.5 , window.innerHeight*0.5 - this.height * 0.5 );
}

CWindow.prototype.move = function ( x , y ) {
    var w = this;
    this.div.style.left = ( x ) + 'px';
    this.div.style.top = ( y ) + 'px';
    w.x = parseInt( w.div.style.left );
    w.y = parseInt( w.div.style.top );
}