/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CWindowMap ( x , y , w , h , game ){
    CWindow.call(this, x , y , w , h , game );
    this.canvas.style.width = '100%';
    this.canvas.style.height = '100%';
};

CWindowMap.prototype = Object.create(CWindow.prototype);
CWindowMap.prototype.constructor = CWindowMap;


CWindowMap.prototype.draw = function ( ){
    CWindow.prototype.draw.apply ( this );
    this.game.map.draw(this.ctx);
};

/**
 * @override
 */
CWindowMap.prototype.resize = function (w,h) {
    w = Math.min (1999, Math.max ( 10, w ) );
    h = Math.min (1999, Math.max ( 10, h ) );
    this.width = w;
    this.height = h;
    this.div.style.width = this.width + "px";
    this.div.style.height = this.height + "px";         
};
