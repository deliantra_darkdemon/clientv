/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CLoader (){
    
}

CLoader.prototype.toLoad = [];
CLoader.prototype.totalCount = 0;
CLoader.prototype.loadedCount = 0;

CLoader.prototype.loadCb = function (){};



CLoader.prototype.add = function (src) {
    src = 'scripts/' + src + '.js?t=' + (new Date());
    this.toLoad.push(src);
    this.totalCount++;
    return this;
};

CLoader.prototype.start = function (cb) {
    var self = this;
    
    self.loadCb = cb || function () { };
    
    for (var i=0;i<this.toLoad.length;++i){
        var sc = document.createElement('script');
        sc.addEventListener('load', function () {
           self.onScriptLoad(sc); 
        });
        sc.src = this.toLoad[i];
        document.head.appendChild(sc);
    }
};

CLoader.prototype.onScriptLoad = function (sc) {  
  if (++this.loadedCount >= this.totalCount) {
      this.loadCb();
  }
};


var Loader = new CLoader();