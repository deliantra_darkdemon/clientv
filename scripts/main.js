/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


//GLOBALS
var G_SERVERURI = 'ws://gameserver.deliantra.net:13327/ws';


//GLOBAL FUNCTIONS

function isUndefined(o) { return typeof (o) === 'undefined' };

function $_ (tag) {
    return document.createElement(tag);
}
//EXTENDS JS
String.prototype.each = function (f, p) {
    for (var i = 0; i < this.length; ++i) {
        f.apply(this, [this[i], i, p]);
    }
};

String.rpad = function ( o, p , s) {
    while ( o.length < p ) {
        o += s;
    }
    return o;
};

Array.prototype.each = function (f, p) {
    for (var i = 0; i < this.length; ++i) {
        f.apply(this, [this[i], i, p]);
    }
};

Array.prototype.contains = function (e) {
    return this.indexOf(e) >= 0 && this.indexOf(e) < this.length;
}

var getJSON=function(a,b,c){var d='undefined'!=typeof XMLHttpRequest?new XMLHttpRequest:new ActiveXObject ( 'Microsoft.XMLHTTP' );d.open ( 'get',a,!0),d.onreadystatechange=function(){var a,e;4==d.readyState&&(a=d.status,200==a?(e=JSON.parse(d.responseText),b&&b(e)):c&&c(a))},d.send()};

/* GLOBAL */

var faces = {};

function init (){
    getJSON("faces.json", function (data) { 
        faces.data = [];
        for (var i = 0;i < data.data.length;++ i){
            faces.data [parseInt(data.data[i].id)] = data.data[i];
        }
         
        scriptLoadComplete ();
    });
}



function scriptLoadComplete () {
    var game = new CGame();
}


window.addEventListener ( 'load', init );
