/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */
function Buffer(buffer, position) {
    this.buffer = buffer;
    this.position = position;
    this.length = buffer.length;
}
Buffer.prototype.unpack_w = function () {
    var num = 0;
    var byte = 0;
    do {
        num *= 128;
        byte = parseInt("" + this.buffer[this.position++]);
        num += byte & 0x7f;
    } while(byte & 0x80);
    return num;
};

Buffer.prototype.readUint32 = function () {
    var ret = 0;
    for (var i = 0; i < 4; ++i) {
        ret *= 256;
        ret += this.buffer[this.position++];
    }
    return ret;
};
Buffer.prototype.readUint8 = function () {
    var ret = 0;
    for (var i = 0; i < 1; ++i) {
        ret *= 256;
        ret += parseInt("" + this.buffer[this.position++]);
    }
    return ret;
};
Buffer.prototype.readUint16 = function () {
    var ret = 0;
    for (var i = 0; i < 2; ++i) {
        ret *= 256;
        ret += this.buffer[this.position++];
    }
    return ret;
};
Buffer.prototype.readString = function (len) {
    var ret = "";
    if (typeof (len) != "undefined") {
        for (var i = 0; i < len; ++i) {
            ret += String.fromCharCode(this.buffer[this.position++]);
        }

        return ret;
    } else {
        while (this.buffer[this.position] != 0) {
            ret += String.fromCharCode(this.buffer[this.position++]);
        }
        return ret;
    }
};