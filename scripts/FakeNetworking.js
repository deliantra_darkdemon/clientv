/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFakeNetworking (game) {
    this.game = game;
}

CFakeNetworking.prototype.connect = function () {
    var self = this;
    this.data = {};
    getJSON("packets.json", function (data) { 
        self.data = data;        
        self.index = 0;
        self.onSockConnected();
    });
    
};

CFakeNetworking.prototype.initReader = function () {
    var self  = this;
    var reader = new FileReader ();
    reader.onloadend = function() {
        self.onMessage(reader.result);  
    };
    return reader;
};


CFakeNetworking.prototype.nextMessage = function () {
    
    var msg = this.data.data[this.index];
    this.index=(this.index+1)%this.data.data.length;
    msg = msg.split(",");
    var omsg = "";
    for (var i = 0; i< msg.length;++i){
        omsg+=String.fromCharCode(parseInt(msg[i]));
    }
    msg = null;
    this.onMessage(omsg);
    omsg = null;
};

CFakeNetworking.prototype.onSockConnected = function () {
    var self = this;
    setInterval(function () {
        self.nextMessage();
    }, 1000/10);
    console.log('connected');
    this.send('version {"clientver":"' + this.game.version + '","client":"deliantra html5 client","osver":"linux","protver":1}');
};

CFakeNetworking.prototype.send = function (s) {
    console.log('send: ' + s);
};

CFakeNetworking.prototype.onMessageEvtReceived = function (d) {
    var data = d.data;
    
    if (data instanceof Blob){        
        var reader = this.initReader();
        reader.readAsBinaryString (data);        
    }else{
        this.onMessage(data);
    }
};

CFakeNetworking.prototype.getFeedName  = function (message) {
    message = message || "";
    var patt=/[0-9a-z_]*[^0-9a-z_]/i;
    var feedName = "" + message.match(patt);
    feedName=feedName.replace(" ", "");
    if (feedName=="null"){
        if (message=="newmap"){
            feedName="newmap";
        }else{
            return null;
        }
    }
    return feedName;
};

CFakeNetworking.prototype.onMessage = function (message) {
    var feedName = this.getFeedName(message+"");
    if (feedName!=null) {
        var feedHandlerObject = FeedHandlerFactory.create(feedName);
        feedHandlerObject.init(feedName, message, this);
        feedHandlerObject.process();    
    }
    
};