/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CMap (game,width, height) {
    
    this.canvas = $_( 'canvas' );
    this.ctx = this.canvas.getContext( '2d' );
    this.canvas.width  = width*64;
    this.canvas.height = height*64;
    
    /**
     * @type CGame
     */
    this.game = game;
    this.width = width;
    this.height = height;
    this.dx = 0;
    this.dy = 0;
    
    this.firstRow = null;
    
    for (var i=0;i<height;++i) {
        var newRow = new CMapRow(width,i,this);
        this.push(newRow);
    }
    
    
}

CMap.prototype.renderTiles = function (pos, tiles) {
    var xx = pos.x*64;
    var yy = pos.y*64;
    
    for (var ii = 0;ii<3;++ii){    
        if (tiles[ii]==0){
            continue;                
        }
        if(faces.data[tiles[ii]].img == null){    
            missing = true;
            this.game.findFace(tiles[ii]);
        }else{
            this.ctx.drawImage ( faces.data[tiles[ii]].img, xx, yy );            
        }
    }
    /*if (missing){
        var self = this;
        //setTimeout((function(tilesx,posx){ return function () { self.renderTiles(posx,tilesx);}})(tiles,pos), 100);
    }*/
}

CMap.prototype.clearRect = function (pos) {
    this.ctx.clearRect(pos.x*64,pos.y*64,64,64);
};

CMap.prototype.get = function (x,y) {
    /**
     * 
     * @type CMapRow
     */
    var currentRow = this.firstRow;
    while (currentRow.firstCell.getXY().y!=y){
        currentRow = currentRow.next;
    }
    return currentRow.get(x);
};

CMap.prototype.modifyScroll = function (x,y) {
    this.dx+=x;
    this.dy+=y;
}

CMap.prototype.setScroll = function (x,y) {
    this.dx=x;
    this.dy=y;
}
CMap.prototype.findLastRow = function (){
    var r = this.firstRow;
    
    if (!r){
        return null;
    }
    
    while(r.hasNext()){
        r = r.next;
    }
    return r;
}

CMap.prototype.shift = function () {
    var r = this.firstRow;
    this.firstRow = r.next;
    this.firstRow.prev = null;
    r.next = null;
    return r;
};

CMap.prototype.push = function (row) {
    var r = this.findLastRow();
    
    if (!r){
        this.firstRow = row;
        row.next = null;
    } else {
        r.next = row;
        row.prev = r;
    }
};

CMap.prototype.pop = function () {
    var r = this.findLastRow();    
    r.prev.next = null;
    r.prev = null;
    return r;
};

CMap.prototype.unshift = function (row) {
    row.next = this.firstRow;
    this.firstRow.prev = row;
    this.firstRow = row;
};

CMap.prototype.clear = function () {
    var r = this.firstRow;
    while (r){
        r.clear();
        r = r.next;
    }        
};

CMap.prototype.scroll = function () {
    
    this.ctx.clearRect(0,0,this.width*64,this.heigth*64);
    
    var tmpDx = this.dx;
    var tmpDy = this.dy;
    
    
    if ( parseInt ( tmpDy ) != 0 ){
        
        var i = 0;
        while (i < tmpDy){        
    
            var oldFirst = this.shift(); 
            oldFirst.clear();
            this.push(oldFirst);        
            
            ++i;        
        }
        i = 0;
        while (tmpDy < i){    
            var oldLast = this.pop(); 
            oldLast.clear();
            this.unshift(oldLast);                    
           --i;
        }
    }
    
    if (tmpDx!=0) {
        i = 0;
        
        while (i < tmpDx) {
    
            var r = this.firstRow;
            while (r){
                r.push(r.shift().clear());
                r = r.next;
            }        
            i++;
        }
        i = 0;
        while (tmpDx < i) {
            
            var r = this.firstRow;
            while (r){
                r.unshift(r.pop().clear());
                r = r.next;
            }
            i--;
        }
    }

    this.setScroll(0,0);
}



CMap.prototype.draw = function (ctx) {
    ctx = ctx || this.game.mapWindow.ctx; //draw to windowMap if ctx not specified
    
    
    //ctx.drawImage(this.canvas, 0, 0);
    
   //return;
    ctx.clearRect ( 0 , 0 , this.width * 64, this.height * 64 );
    var findFace = this.game.findFace.bind ( this.game );
    var missing = false;
    for (var x = 0 ; x < this.width ; ++x){
        for (var y = 0 ; y < this.height ; ++y){
            var cell = this.get ( x , y );
            for (var z = 0 ; z < cell.tiles.length ; ++z){
                if (cell.tiles[z]){
                    var img;
                    if ( ( img = faces.data [ cell.tiles [ z ] ].img ) == null ){                            
                        findFace ( cell.tiles [ z ] );
                        missing = true;
                    }else{
                        ctx.drawImage(img, x*64, y*64 );
                    }
                }
            }
        }
    }    
    if ( missing ){
        var self = this;
        setTimeout( function ( ) { self.draw ( ctx ); } , 1000);
    }   
};

