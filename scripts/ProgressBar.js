/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CProgressBar  (game,foreColor,backColor, width) {
    this.x = 0;
    this.y = 0;    
    this.width = width || 800;
    this.height = 20;
    this.currentVal = parseInt(Math.random()*100);
    this.maxVal = 100;
    this.foreColor = foreColor;
    this.backColor = backColor;
    this.rotation = 0;
    this.game = game;
}


CProgressBar.prototype.draw = function ( ctx ){
    
    ctx.fillStyle = this.foreColor || 'blue';
    ctx.fillRect(0, 0, 1, 1);
    var data = ctx.getImageData(0, 0, 1, 1);
    ctx.clearRect(0, 0, 1, 1);
    var r = 255 - data.data[0];
    var g = 255 - data.data[1];
    var b = 255 - data.data[2];

    ctx.fillStyle = this.backColor || 'lightblue';
    ctx.fillRect(0, 0, 1, 1);
    var data = ctx.getImageData(0, 0, 1, 1);
    ctx.clearRect(0, 0, 1, 1);
    
    
    var r = parseInt((r + 255 - data.data[0])*0.5);
    var g = parseInt((g + 255 - data.data[1])*0.5);
    var b = parseInt((b + 255 - data.data[2])*0.5);
    data = null;
    ctx.save();
    ctx.translate(this.x,this.y);
    ctx.rotate(this.rotation);
    
    if (this.percent) {
        var w = Math.min(this.width*this.percent,this.width);        
        ctx.fillStyle = this.backColor || 'lightblue';
        ctx.fillRect(0,0,this.width,this.height); 
        ctx.fillStyle = this.foreColor || 'blue';
        ctx.fillRect(0,0,w,this.height);
        ctx.strokeStyle='yellow';
        ctx.strokeRect(0,0,this.width,this.height); 
        var descText = ""+this.currentVal + " (" + parseInt(this.percent*100) + "%)";
        w = ctx.measureText(descText).width;
        ctx.fillStyle='rgb(' + r + ',' + g + ',' + b +')';
        ctx.fillText(descText,this.width*0.5-w*0.5,13);
    }else {
        var w = Math.min((this.currentVal/this.maxVal)*this.width,this.width);        
        ctx.fillStyle = this.backColor || 'lightblue';
        ctx.fillRect(0,0,this.width,this.height); 
        ctx.fillStyle = this.foreColor || 'blue';
        ctx.fillRect(0,0,w,this.height);
        ctx.strokeStyle='yellow';
        ctx.strokeRect(0,0,this.width,this.height); 
        var descText = ""+this.currentVal + " (" + parseInt((this.currentVal/this.maxVal)*100) + "%)";
        w = ctx.measureText(descText).width;
        ctx.fillStyle='rgb(' + r + ',' + g + ',' + b +')';
        ctx.fillText(descText,this.width*0.5-w*0.5,13);
    }
    ctx.restore();
};