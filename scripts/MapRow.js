/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CMapRow (size,y,map) {    
    CLinkedList.call(this);    
    this.map = map;
    this.length = 0;
    this.firstCell = null;
    
    while (this.length < size){
        this.push ( new CMapCell(this));
        ++this.length;
    }    
};

CMapRow.prototype = Object.create(CLinkedList.prototype);
CMapRow.prototype.constructor = CMapRow;

CMapRow.prototype.clear = function ( ){
    var c = this.firstCell;
    while (c){
        c.clear();
        c=c.next;
    }
};


CMapRow.prototype.findLast = function ( ) {
    var r = this.firstCell;
    while ( r && r.hasNext ( ) ){
        r = r.next;
    }
    return r;
}

CMapRow.prototype.shift = function () {
    var r = this.firstCell;
    this.firstCell = r.next;
    this.firstCell.prev = null;
    r.next = null;
    return r;
};

CMapRow.prototype.push = function (row) {
    var r = this.findLast();
    if (r==null){
        this.firstCell= row;
        this.firstCell.next =null;
    
    } else {
        r.next = row;
        row.prev = r;        
    }
    
};

CMapRow.prototype.pop = function () {
    var r = this.findLast();    
    r.prev.next = null;
    r.prev = null;
    return r;
};

CMapRow.prototype.unshift = function (row) {
    row.next = this.firstCell;
    this.firstCell.prev = row;
    this.firstCell = row;
};

CMapRow.prototype.process = function () {
    //CLinkedList.prototype.process.apply(this);        
};

var xxx = 1;

CMapRow.prototype.get = function (x) {
    /**
     * 
     * @type CMapCell;
     */
    var currentCell = this.firstCell;
//    console.log(xxx + " " + this.firstCell.getXY());
//    console.log(xxx + " targetX " + x);
//    ++xxx;
    while(currentCell.getXY().x != x){ 
        //console.log(currentCell.getXY().x);
        currentCell = currentCell.next;
    }
//    console.log(currentCell.getXY().x);
    if (currentCell.getXY().x == x){
       return currentCell;
    }
    return null;
};
