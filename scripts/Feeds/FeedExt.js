/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFeedExt (name, data, networking) {
    CFeedBase.call(this, name, data, networking);    
};

CFeedExt.prototype = Object.create(CFeedBase.prototype);
CFeedExt.prototype.constructor = CFeedExt;


CFeedExt.prototype.process = function () {
    CFeedBase.prototype.process.apply(this);    
    
    if (this.data.indexOf('nonces')>0){ //unknown
        return;
    }
    
    if (this.data.indexOf ("channel_info")>0){
        var channel_info = JSON.parse(this.data.substr(4))[1];
        channel_info.messages = [];
        
        if (!channel_info.id){
            return;
        }
        
        for ( var i = 0 ; i < this.game.channels.length ; ++i) {
            if ( this.game.channels[ i ].id == channel_info.id){
                return;
            }
        }        
        this.game.channels.push(channel_info);
        
        return true;
    }else{

        var o = JSON.parse(this.data.substr(4));
        var replyID = parseInt(o[0].split("-")[1]);

        if (replyID == 101){
            /*baseURL = o[1];
            baseURL = "http://gameserver.deliantra.net:13327" + baseURL + "";*/
            this.networking.send('exti ["resource",'+this.game.getNextExtId()+',"exp_table"]');


        } else if ( replyID == 102){            
            console.log(o);
            this.game.faceIDExpTable = parseInt(o[1]);            
            this.networking.send('exti ["resource",'+this.game.getNextExtId() + ', "skill_info", "spell_paths","command_help"]');
            
        } else if ( replyID == 103 ) {
            this.game.faceIDSkillNames = parseInt ( o [ 1 ] );
            this.game.faceIDSpellPaths = parseInt ( o [ 2 ] );
            this.game.faceIDCommandsAll = parseInt ( o [ 3 ] );
            this.networking.send("addme");

        }
        else{

        }
    }    
    //console.log(this.data);
};

