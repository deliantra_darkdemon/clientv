/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFeedItem2 (name, data, networking) {
    CFeedBase.call(this, name, data, networking);    
};

CFeedItem2.prototype = Object.create(CFeedBase.prototype);
CFeedItem2.prototype.constructor = CFeedItem2;


CFeedItem2.prototype.process = function () {
    CFeedBase.prototype.process.apply(this);    
    
    var buff = new Buffer(this.getDataArray());
    buff.position = 6;
    var container = buff.readUint32();
    var containerObject = this.game.containers.find(container) || this.game.containers.add(container); 
    
    while (buff.position<buff.length){
        var item = new CItem();

        item.itemId = buff.readUint32();

        item.flags =  buff.readUint32();
        item.weight = buff.readUint32();
        item.face = buff.readUint32();
        var len = (buff.readUint8());

        var names = (buff.readString(len)).split(String.fromCharCode(0));
        item.name = names[0];
        item.namepl = "";

        if (names.length>1)
            item.namepl = names[1];

        item.animation_id = buff.readUint16();
        item.anim_speed = buff.readUint8();
        item.nr_of = buff.readUint32();
        item.clientType = buff.readUint16();
        item.container = container;

        containerObject.add(item);        
    }    
};

