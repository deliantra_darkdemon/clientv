/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFeedVersion (name, data, networking) {
    CFeedBase.call(this, name, data, networking);    
};

CFeedVersion.prototype = Object.create(CFeedBase.prototype);
CFeedVersion.prototype.constructor = CFeedVersion;


CFeedVersion.prototype.process = function () {
    CFeedBase.prototype.process.apply(this);
    var mapsize = (this.game.map.width+0) + "x" + (this.game.map.height+0);
    this.networking.send('setup tileset 0 excmd 1 smoothing 1 mapinfocmd 1 facecache 1 newmapcmd 1 extmap 1 fxix 3 darkness 1 extcmd 2 msg 2 frag 0 map1acmd 1 spellmon 2 itemcmd 2 exp64 1 widget 1 lzf 0 mapsize ' + mapsize);
};

