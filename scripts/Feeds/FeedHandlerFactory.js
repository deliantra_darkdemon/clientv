/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CFeedHandlerFactory () {
    this.instances = { 'version' : this.getInstance ( "CFeedVersion" ),
        'setup' : this.getInstance ( "CFeedSetup" ),
        'ext' : this.getInstance ( "CFeedExt" ),
        'query' : this.getInstance ( "CFeedQuery" ),
        'map1a' : this.getInstance ( "CFeedMap1a" ),
        'map_scroll' : this.getInstance ( "CFeedMapScroll" ),
        'newmap' : this.getInstance ( "CFeedNewMap" ),
        'stats' : this.getInstance ( "CFeedStats" ),
        'upditem' : this.getInstance ( "CFeedUpdItem" ),
        'item2' : this.getInstance ( "CFeedItem2" ),
        'delinv' : this.getInstance ( "CFeedDelInv" ),
        'fx' : this.getInstance ( "CFeedFx" ),
        'msg' : this.getInstance ( "CFeedMsg" ),
    };
    this.default = this.getInstance ( "CFeedUnhandled" );
}

CFeedHandlerFactory.prototype.create = function (feedName) {
    if ( this.instances [ feedName ] ) {
        return this.instances [ feedName ];
    }
    return this.default;
};

CFeedHandlerFactory.prototype.getInstance = function (name) {
    return new window[name];
};

var FeedHandlerFactory = new CFeedHandlerFactory ( ) ;