/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFeedStats (name, data, networking) {
    CFeedBase.call(this, name, data, networking);    
};

CFeedStats.prototype = Object.create(CFeedBase.prototype);
CFeedStats.prototype.constructor = CFeedStats;


CFeedStats.prototype.process = function () {
    CFeedBase.prototype.process.apply(this);    
    var buff = new Buffer(this.getDataArray());
    buff.position = "stats".length + 1;
    var face = this.game.faces.find ( this.game.faceIDSkillNames );            
    var skillNames = face.data || [];
    while (buff.position < buff.length){
        var stat = buff.readUint8();
        var value;
        if (stat_32bit.contains(stat)){
            value = buff.readUint32();
        }else if (stat == CS_STAT_SPEED||stat==CS_STAT_WEAP_SP){
            value = (1/FLOAT_MULTF) * buff.readUint32();
        }else if (stat==CS_STAT_RANGE||stat==CS_STAT_TITLE){
            var len = buff.readUint8();
            value = buff.readString(len);
        }else if (stat == CS_STAT_EXP64){
            var hi = buff.readUint32();
            var lo = buff.readUint32();
            value = hi * Math.pow(2,32) + lo;
        }else if (stat>=CS_STAT_SKILLINFO && stat < CS_STAT_SKILLINFO + CS_NUM_SKILLS){
            var lvl = buff.readUint8();
            var hi = buff.readUint32();
            var lo = buff.readUint32();
            var exp = hi * Math.pow(2,32) + lo;
            var previousExp = this.game.myStats[stat] instanceof Array ? this.game.myStats[stat][1] : 0;
            if (previousExp < exp){
                console.log("gained " + (exp - previousExp) + " exp in skill " + (skillNames.length?skillNames[stat-CS_STAT_SKILLINFO][0] : ""));
            }
            value = [lvl,exp];
        }else{
            value = buff.readUint16();
            if (value > 60000){
                value -= 65536;
            }
        }
        this.game.myStats [stat] = value;
        //stats_update (stat);
    }

};

