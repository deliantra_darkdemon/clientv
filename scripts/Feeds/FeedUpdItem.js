/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFeedUpdItem (name, data, networking) {
    CFeedBase.call(this, name, data, networking);    
};

CFeedUpdItem.prototype = Object.create(CFeedBase.prototype);
CFeedUpdItem.prototype.constructor = CFeedUpdItem;


CFeedUpdItem.prototype.process = function () {
    CFeedBase.prototype.process.apply(this);    
    return;
    var buff=new Buffer(this.getDataArray());
    buff.position = "upditem ".length;
    var flags = buff.readUint8();
    var tag = buff.readUint32();

    var itm = getItemById(tag);
    if (!itm){
        return;
    }

    if (flags & UPD_LOCATION) {
        removeItemEx(tag);

        var container = buff.readUint32();
        if (container==player.tag){
            myItems.push(itm);
        }else if (container==0){
            floorItems.push(itm);
        }else{
            if (!typof(containers["container_" + container])==typeof([])){
                containers["container_" + container] = [];
            }
            containers["container_" + container].push (itm);
        }
    }
    if (flags & UPD_FLAGS)
        itm.flags = buff.readUint32();
    if (flags & UPD_WEIGHT)
        itm.weight = buff.readUint32();//$item->{weight} = unpack "l", pack "L", unpack "N", substr $data, 0, 4, "" ;
    if (flags & UPD_FACE)
        itm.face = buff.readUint32();


    if((itm.flags&F_OPEN)){
            secondaryContainer = tag;
            updateItemsList();
    }
    
    //TODO: finish it!
    /*
     if ($flags & UPD_NAME) {
     my $len = unpack "C", substr $data, 0, 1, "";

     my $names = substr $data, 0, $len, "";
     utf8::decode $names;
     @$item{qw(name name_pl)} = split /\x00/, $names;
     }

     $item->{anim}   = unpack "n", substr $data, 0, 2, "" if $flags & UPD_ANIM;
     $item->{animspeed} = TICK * unpack "C", substr $data, 0, 1, "" if $flags & UPD_ANIMSPEED;
     $item->{nrof}   = unpack "N", substr $data, 0, 4, "" if $flags & UPD_NROF;

     $item->{mtime} = time;

     if ($item->{tag} == $self->{player}{tag}) {
     $self->player_update ($self->{player} = $item);
     } else {
     $self->item_update ($item);
     }
     */

};

