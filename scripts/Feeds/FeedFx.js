/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFeedFx (name, data, networking) {
    CFeedBase.call(this, name, data, networking);    
};

CFeedFx.prototype = Object.create(CFeedBase.prototype);
CFeedFx.prototype.constructor = CFeedFx;


CFeedFx.prototype.process = function () {
    CFeedBase.prototype.process.apply(this);    
    
    var buff = new Buffer(this.getDataArray());
    buff.position = this.name.length+1;
    
    var out="";
    var type = 0;
    var nameX = "";
    var len;
    while (buff.position<buff.length){
        nameX = "";
        var id = parseInt(buff.unpack_w());
		
        if (id == 0){
            buff.readUint8();
            type = buff.unpack_w();
            continue;
        }
        len = buff.readUint8();

        for (var i = 0;i<len;++i){
            var c = parseInt(buff.readUint8());
            out = "";
            out+="0123456789abcdef"[c&0xf];
            out= "0123456789abcdef"[c>>4]+out;
            nameX+=out;
        }
        /**
         * 
         * @type CFace
         */
        var face = this.game.faces.find ( id ) || this.game.faces.push ( new CFace ( ) );        
        face.id = id;
        face.hash = nameX;
        face.type = type;
        face.load ( );
        face = null;
    }

};

