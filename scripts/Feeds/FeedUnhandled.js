/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFeedUnhandled (name, data, networking) {
    CFeedBase.call(this, name, data, networking);    
};

CFeedUnhandled.prototype = Object.create(CFeedBase.prototype);
CFeedUnhandled.prototype.constructor = CFeedUnhandled;


CFeedUnhandled.prototype.process = function () {
    CFeedBase.prototype.process.apply(this);  
//    console.log("Unhandled feed!!!");
//    console.log(this.name);
//    console.log(this.data);
};

