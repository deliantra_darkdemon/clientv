/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFeedMap1a (name , data , networking ) {
    CFeedBase.call ( this , name , data , networking );    
};

CFeedMap1a.prototype = Object.create ( CFeedBase.prototype );
CFeedMap1a.prototype.constructor = CFeedMap1a;

CFeedMap1a.prototype.process = function ( ) {
    CFeedBase.prototype.process.apply ( this );
    this.game.map.scroll();
    var x, y;

    var index = 6;
    var data = this.getDataArray();

    while (index < data.length - 2) {
        var flags = (data[index++] << 8);
        flags += data[index++];
        x = ((flags >> 10) & 63);
        y = ((flags >> 4) & 63);

        /**
         * 
         * @type CMapCell
         */
        var cell = this.game.map.get(x, y);
        //console.log(cell.x,cell.y);


        if (!(flags & 15)) {
            cell.clear();
            //ctx.clearRect(x<<6,(y<<6),64,64);
            continue;
        }

        if (cell.darkness == 0) {
            cell.darkness = 256;

        }

        if ((flags & 8)) {
            do {
                var cmd = 0;
                var ext = 0;

                ext = data[index++];
                cmd = ext & 0x7f;
                if (cmd < 4) {
                    cell.darkness = 255 - ext * 64 + 1;
                } else if (cmd == 5) {
                    cell.stat_width = 1;
                    cell.stat_hp = data[index++];
                } else if (cmd == 6) {// monster width{
                    cell.stat_width = data[index++] + 1;
                } else if (cmd == 0x47) {
                    if (data[index] == 1)
                        cell.player = data [index + 1];
                    else if (data[index] == 2)
                        cell.player = data [index + 2] + (data [index + 1] << 8);
                    else if (data[index] == 3)
                        cell.player = data [index + 3] + (data [index + 2] << 8) + (data [index + 1] << 16);
                    else if (data[index] == 4)
                        cell.player = data [index + 4] + (data [index + 3] << 8) + (data [index + 2] << 16) + (data [index + 1] << 24);

                    index += data[index] + 1;
                } else if (cmd == 8) // cell flags
                    cell.flags = data[index++];
                else if (ext & 0x40) // unknown, multibyte => skip
                    index += data[index] + 1;
                else
                    index++;
            } while (ext & 0x80);
        }

        for (var z = 0; z <= 2; ++z)
            if ((flags & (4 >> z)))
            {
                var face = (data [ index++ ] << 8);
                face += data [ index++ ];
                cell.tiles [ z ] = face;
            }
        //cell.draw ( );
    }
    this.game.map.draw();    
};

