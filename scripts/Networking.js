/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CNetworking (game) {
    this.game = game;
}

CNetworking.prototype.connect = function () {
    var self = this;
    this.sock = new CSocket();
    this.sock.addHandler('open' , function () { self.onSockConnected(); });
    this.sock.addHandler('message' , function (d) { self.onMessageEvtReceived(d); });
    this.readers = [];
    for (var i = 0 ; i < 10 ; ++i ){
        //console.log ( "CREATE READER ");
        this.readers.push ( this.createReader ( ) );
    };
};

CNetworking.prototype.createReader = function ( ){
    var self  = this;
    var reader = new FileReader ( );
    reader.onloadend = function ( ){
        self.onMessage ( reader.result );  
    };
    return reader;
};

CNetworking.prototype.initReader = function () {
    for ( var i = 0 ; i < this.readers.length ; ++i ){        
        if ( this.readers [ i ].readyState == 2 || this.readers [ i ].readyState == 0){//EMPTY OR DONE
            return this.readers [ i ];
        }
    }
    
    this.readers.push ( this.createReader ( ) );
    return this.readers [ this.readers.length - 1];
};

CNetworking.prototype.onSockConnected = function () {
    console.log('connected');
    this.send('version {"clientver":"' + this.game.version + '","client":"deliantra html5 client","osver":"linux","protver":1}');
};

CNetworking.prototype.send = function ( s ) {
    this.sock.send ( s );
    //console.log('send: ' + s);
};

CNetworking.prototype.onMessageEvtReceived = function ( d ) {
    var data = d.data;
    
    if ( data instanceof Blob ){        
        var reader = this.initReader ();
        reader.readAsBinaryString ( data );  
        reader = null;
    }else{
        this.onMessage ( data );
    }
};

CNetworking.prototype.getFeedName  = function ( message ) {
    message = message || "";
    var patt=/[0-9a-z_]*[^0-9a-z_]/i;
    var feedName = "" + message.match ( patt );
    feedName = feedName.replace ( " " , "" );
    if ( feedName == "null" ){
        if ( message=="newmap" ){
            feedName = "newmap";
        }else{
            return null;
        }
    }
    return feedName;
};

CNetworking.prototype.onMessage = function ( message ) {
    var feedName = this.getFeedName( message+"" );
    if ( feedName != null) {
        var feedHandlerObject = FeedHandlerFactory.create ( feedName );
        feedHandlerObject.init ( feedName , message , this);
        feedHandlerObject.process ( );    
    }
    
};