/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CKeyboard ( game ) {
    this.game = game;
    this.keysDown = [];
      
    this.onKeyPressCb = [];
    this.handlersDown = [];
    this.handlersUp = [];
    var self = this; 
    window.addEventListener ( 'keydown', function ( e ) { return self.onKeyPress( e );} );
    window.addEventListener ( 'keyup', function ( e ) { return self.onKeyPressRelease( e );} );
}

CKeyboard.prototype.onKeyPress = function ( e ) {
    
    if (this.game.inputText) {
        return true;
    }
    
    this.keysDown [ e.keyCode ] = true;
    e.preventDefault();  
    for ( var i = 0 ; i < this.onKeyPressCb.length ; ++i){
        this.onKeyPressCb [ i ]( e );
    }
    if ( this.handlersDown [ e.keyCode ] ){
        var handlers = this.handlersDown [ e.keyCode ];
        for ( var i = 0 ; i < handlers.length ; ++i ){
            handlers[i]();
        }
    }
    //console.log ( e.keyCode );
    return false;
};

CKeyboard.prototype.registerKeyDownHandler = function ( key , cb ){
    var handlers = this.handlersDown [ key ] = this.handlersDown [ key ] || [];
    handlers.push ( cb );
};

CKeyboard.prototype.registerKeyUpHandler = function ( key , cb ){
    var handlers = this.handlersUp [ key ] = this.handlersUp [ key ] || [];
    handlers.push ( cb );
};

CKeyboard.prototype.onKeyPressRelease = function (e) {
    if (this.game.inputText) {
        return true;
    }
    this.keysDown [ e.keyCode ] = false;
    if ( this.handlersUp [ e.keyCode ] ){
        var handlers = this.handlersUp [ e.keyCode ];
        for ( var i = 0 ; i < handlers.length ; ++i ){
            handlers[i]();
        }
    }
    e.preventDefault ( );
    return false;
};

CKeyboard.prototype.getKey = function ( code ) {
    return this.keysDown [ code ] || false;    
};

CKeyboard.prototype.getKeyOnce = function ( code ) {
    var r = this.keysDown [ code ] || false;
    this.keysDown [ code ] = false;
    return r;
};

CKeyboard.prototype.tick = function ( ) {
    
};

CKeyboard.KEYS = {
    LEFT      : 39,
    RIGHT     : 37,
    UP        : 38,
    DOWN      : 40,
    SPACE     : 32,
    BACKSPACE : 8,
    ENTER     : 13,
    CTR       : 17,
    TAB       : 9,
    ALT       : 18,
    SHIFT     : 16,
    F3        : 114,
};