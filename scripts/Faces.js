/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CFaces () {    
    CLinkedListHead.call(this);
};



CFaces.prototype = Object.create(CLinkedListHead.prototype);
CFaces.prototype.constructor = CFaces;

CFaces.prototype.find = function ( id ){
    var e = this.first;
    while ( e ){
        if ( e.id == id){
            return e;
        }
        e = e.next;
    }
    return null;
};

CFaces.prototype.push = function ( o ){
    CLinkedListHead.prototype.push.call(this, o);
    return o;
};



