/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */

function CSocket () {
    var ws = WebSocket || MozWebSocket;
    var self = this;    
    this.socket = new ws(G_SERVERURI);
    this.socket.onopen = function () { self.onOpen(); };
    this.socket.onclose = function () { self.onClose(); };
    this.socket.onmessage = function (evt) { self.onMessage(evt); };
    this.socket.onerror = function () { self.onError(); }; 
    
};

CSocket.prototype.events = {  'open' : [],
                    'close' : [],
                    'error' : [],
                    'message' : [],
                };

CSocket.prototype.send = function (s) {
    this.socket.send(s);
}

CSocket.prototype.addHandler = function (evt,cb) {
    if (isUndefined(this.events[evt])){
        return;
    }
    this.events[evt].push (cb);
    return this;
};


CSocket.prototype.onOpen = function () {
    this.fireEvent('open');
};

CSocket.prototype.onClose = function () {
    this.fireEvent('close');
};

CSocket.prototype.onMessage = function (evt) {
    this.fireEvent('message', evt);
};

CSocket.prototype.onError = function () {
    this.fireEvent('error');
};

CSocket.prototype.fireEvent = function (evt, o) {
    if (isUndefined(this.events[evt])){
        return;
    }
    for (var i = 0; i < this.events[evt].length; ++i){
        this.events[evt][i](o);
    }
};
