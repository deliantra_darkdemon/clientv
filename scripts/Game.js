/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CGame ( ){
    this._nextExtId = 100;
    /**
     * @type Cmap
     */
    this.map = new CMap(this,9,9);    
    this.version = "0.003";
    /**
     * @type CNetworking
     */
    this.networking = new CNetworking(this);
    //this.networking = (confirm("playback ?" ) === true) ? new CFakeNetworking(this) : new CNetworking(this);
    /**
     * @type CKeyboard
     */
    this.keyboard = new CKeyboard(this);  
    this.containers = new CContainers( );
    this.canvas = $_('canvas');
    this.ctx = this.canvas.getContext("2d");
    this.canvas.width = window.innerWidth-5;
    this.canvas.height = 50;
    document.body.appendChild(this.canvas);
    
    this.ticks = 0;
    this.lastCommand = 0;
    this.myStats = [];
    this.faces = new CFaces();
    /**
     * @type CCommandConsole
     */
    this.commandConsole = new CCommandConsole(this);
    
    /**
     * @type CWindowFloor
     */
    this.floorWindow = new CWindowFloor ( 0 , 0 , 200 , 100 , this );
    
    /**
     * @type CWindowBars
     */
    this.barsWindow = new CWindowBars ( 100 , 0 , 840 , 150 , this );
    
    /**
     * @type CWindowMap
     */
    this.mapWindow = new CWindowMap ( 0 , 330 , this.map.width * 64 , this.map.height * 64 , this );
    
    /**
     * @type CWindowSkills
     */
    this.skillsWindow = new CWindowSkills ( 0 , 330 , this.map.width * 64 , this.map.height * 64 , this );
    this.skillsWindow.show ( false );
    /**
     * @type CWindowContainer
     */
    this.containerWindow = new CWindowContainer ( 0 , 430 , this.map.width * 64 , this.map.height * 64 , this , null);
    
    
    /**
     * @type CWindowMessages
     */
    this.messagesWindow = new CWindowMessages ( 0 , 430 , this.map.width * 64 , 4 * 64 , this , null);
    
    this.containerWindow.show(false);
    this.channels = [JSON.parse('{"tooltip":"Server info","title":"Info","id":"info","reply":"","messages":[]}')];
    
    [ this.containerWindow , this.messagesWindow, this.skillsWindow , this.mapWindow , this.barsWindow , this.floorWindow ].each ( function ( o ) {
        o.show ( false );
    });
    
    this.loginWindow = new CWindowLogin ( 0 , 0 , 440, 85 , this , null);
    this.loginWindow.center();
    
    var self = this;
    setInterval(function ( ){ self.mainLoop( ); }, 1000/40);    
    this.keyboard.onKeyPressCb.push ( function ( e ){
        self.onKeyPress ( e );
    } );
    
    this.keyboard.registerKeyDownHandler( CKeyboard.KEYS.CTR , function ( ) {
        self.toggleRun ( true );
    } );
    
    this.keyboard.registerKeyUpHandler( CKeyboard.KEYS.CTR , function ( ) {
        self.toggleRun ( false );
    } );
    
    this.keyboard.registerKeyDownHandler( CKeyboard.KEYS.SHIFT , function ( ) {
        self.toggleFire ( true );
    } );
    
    this.keyboard.registerKeyUpHandler( CKeyboard.KEYS.SHIFT , function ( ) {
        self.toggleFire ( false );
    } );
    
    this.keyboard.registerKeyUpHandler( CKeyboard.KEYS.TAB , function ( ) {
        self.containerWindow.container = self.containers.first;
        self.containerWindow.toggleShow();
    } );
    
    this.keyboard.registerKeyUpHandler( CKeyboard.KEYS.F3 , function ( ) {
        self.skillsWindow.toggleShow();
    } );
            
    
}

CGame.prototype.showUI = function ( ) {
    [ this.messagesWindow , this.mapWindow , this.barsWindow , this.floorWindow ].each ( function ( o ) {
        o.show ( true );
    });
    this.inputText = false;
    this.loginWindow.show ( false );
};

CGame.prototype.toggleFire = function ( b ) {
    if ( ( ! this.isFiring ) && b ){
        this.isFiring = true;
        this.networking.send ( "command fire" );
    }else if ( this.isFiring && !b ){
        this.isFiring = false;
        this.lastDir = null;
        this.networking.send ( "command fire_stop" );
    }
};
CGame.prototype.toggleRun = function ( b ) {
    if ( ( ! this.isRunning ) && b ){
        this.isRunning = true;
        this.networking.send ( "command run" );
    }else if ( this.isRunning && !b ){
        this.isRunning = false;
        this.lastDir = null;
        this.networking.send ( "command run_stop" );
    }
};

CGame.prototype.getNextExtId = function ( ){
    return ++this._nextExtId;
}

CGame.prototype.start = function ( ){
    this.networking.connect( );    
};

CGame.prototype.findFace = function ( id ){
    var o = faces.data [ parseInt ( id ) ];
    
    if (o.img){
        return o.img;
    }else{
        if (o.loading == true){
            return null;
        }
        o.loading = true;
        var img = new Image ( );
        img.onload = function ( ){
            faces.data [ id ].img = this;
        };
        img.src="http://gameserver.deliantra.net:13327/" + faces.data [ id ].csum;
        return null;
    }    
};

CGame.prototype.onKeyPress = function ( e ){
    //console.log(e);
    this.commandConsole.onInput ( e );
};

CGame.prototype.findChannel = function ( id ) {
    for (var i = 0;i<this.channels.length;++i){
        if (this.channels[i].id == id ){
            this.channels[i].active = true; //we will surely add msg if we look for channel info
            return this.channels[i];
        }
    }
    return {messages:[]};
};

CGame.prototype.mainLoop = function ( ){
    
    this.ctx.clearRect ( 0 , 0 , this.canvas.width, this.canvas.height ); // without it command console was messed up
    
    
    this.commandConsole.draw ( );
    this.floorWindow.draw ( );
    this.barsWindow.draw ( );
    this.messagesWindow.draw ( );
    
    if ( this.containerWindow.isShown() ) {
        this.containerWindow.draw ( );
    };
    
    if ( this.skillsWindow.isShown() ) {
        this.skillsWindow.draw ( );
    }
    
    //this.mapWindow.draw ( ); /* CALLED BY MAP1A!!! */
    
    this.ticks++;
    this.keyboard.tick( );
    
    if (this.ticks > this.lastCommand + 4){        
    } else{
        return;
    }
    var dirAsName = ['east', 'west', 'north', 'south'];
    
    var dirName = "";
    
    if (this.keyboard.getKeyOnce(CKeyboard.KEYS.LEFT)){        
        dirName = 'east';
        this.lastCommand = this.ticks;
    }else if (this.keyboard.getKeyOnce(CKeyboard.KEYS.RIGHT)){
        dirName = "west";
        this.lastCommand = this.ticks;
    }else if (this.keyboard.getKeyOnce(CKeyboard.KEYS.DOWN)){
        dirName = "south";
        this.lastCommand = this.ticks;
    }else if (this.keyboard.getKeyOnce(CKeyboard.KEYS.UP)){
        dirName = "north";
        this.lastCommand = this.ticks;
    }else if (this.keyboard.getKeyOnce(CKeyboard.KEYS.SPACE)){
        this.networking.send('command apply');
        this.lastCommand = this.ticks;
    }
    if (dirName.length){
        if (this.isRunning){
            if (!this.lastDir || (this.lastDir && this.lastDir!=dirName)){
                this.networking.send('command ' + dirName);
                this.lastDir = dirName;
            }
        }else{
           this.networking.send('command ' + dirName);
           this.lastDir = null;
        }
    }
};