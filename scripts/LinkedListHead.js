/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CLinkedListHead () {
   this.first = null; 
};


CLinkedListHead.prototype.clear = function () {
   this.first = null;
};


CLinkedListHead.prototype.findLast = function (){
    var r = this.first;
    while(r&&r.hasNext()){
        r = r.next;
    }
    return r;
}

CLinkedListHead.prototype.shift = function () {
    var r = this.first;
    this.first = r.next;
    this.first.prev = null;
    r.next = null;
    return r;
};

CLinkedListHead.prototype.push = function (row) {
    var r = this.findLast();
    if (r){
        r.next = row;
        row.prev = r;
        return r;
    } else {
      this.first = row;  
    }
    
};

CLinkedListHead.prototype.pop = function () {
    var r = this.findLast();  
    if (r) {
        r.prev.next = null;
        r.prev = null;
        return r;
    }
    else return null;
    
};

CLinkedListHead.prototype.unshift = function (row) {
    row.next = this.first;
    this.first.prev = row;
    this.first = row;
};



CLinkedListHead.prototype.get = function (x) {
    var current = this.first;
    for (var i=1;i<=x;++i) {
        if (!current.hasNext()){
            break;
        }
        current = current.next;
    }
    return current;
};
