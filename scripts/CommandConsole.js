/* 
 *  This file is part of Deliantra clientV.
 * 
 *  Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 * 
 *  Deliantra clientV is free software: you can redistribute it and/or
 *  modify it under the terms of the Affero GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the Affero GNU General Public Licens
 *  and the GNU General Public License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 * 
 *  The authors can be reached via e-mail to <support@deliantra.net>
 */


function CCommandConsole  (game) {
    this.x = 0;
    this.y = 0;
    this.command = "";
    this.previousCommand = "";
    this.game = game;
}

CCommandConsole.prototype.onInput = function (e) {
    if (e.key.length == 1){
        if (e.key == ' ') {
            if (this.command.length){
                this.command+=e.key;
            }
        } else if (e.key == '.' && this.command.length == 0 && this.previousCommand.length ) {
            this.game.networking.send ( "command " + this.previousCommand );
        } else {
            this.command += e.key;
        }
        
    }else {
        if (e.keyCode == CKeyboard.KEYS.BACKSPACE){
            this.command = this.command.substr(0,this.command.length-1);
        } else if (e.keyCode == CKeyboard.KEYS.ENTER){
            if (this.command.length) {
                this.previousCommand = this.command;
                this.game.networking.send("command " + this.command);
                this.command = "";
            }
                
        }
    }
    //console.log(e);
};

CCommandConsole.prototype.draw = function () {
    
    if (this.command.length) {
        var ctx = this.game.ctx;
        var w = ctx.measureText(this.command).width+20;
        var gameCanvasWidth = this.game.canvas.width;
        ctx.fillStyle = 'blue';
        ctx.fillRect(gameCanvasWidth*0.5 - w*0.5,0,w,20);
        ctx.fillStyle = 'white';
        ctx.fillText(this.command,gameCanvasWidth*0.5 - w*0.5+10,12);
        ctx = null;
    }
    
    
};

CCommandConsole.prototype.getPrev = function () {
    
};

CCommandConsole.prototype.hasPrev = function () {
    
};